#!/usr/bin/env python
# -*- coding=utf-8 -*-

import time
import datetime
import Cookie


body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Żądanie przyszło o %s.<br>
        <br>
        Ostatnia twoja wizyta miała miejsce %s.<br>
    </body>
</html>
"""


def application(environ, start_response):
    cookie = Cookie.SimpleCookie()
    lastvisit = 'BRAK'

    if 'HTTP_COOKIE' in environ.keys():
        cookie.load(environ['HTTP_COOKIE'])
        lastvisit = cookie['lastvisit'].value

    response_body = body % (
        environ.get('SERVER_NAME', 'Unset'),  # nazwa serwera
        environ.get('SERVER_ADDR', 'BRAK'),  # IP serwera
        environ.get('SERVER_PORT', 'BRAK'),  # port serwera
        environ.get('REMOTE_USER', 'BRAK'),  # nazwa klienta
        environ.get('REMOTE_ADDR', 'BRAK'),  # IP klienta
        environ.get('SERVER_PORT', 'BRAK'),  # port klienta
        environ.get('SCRIPT_NAME', 'BRAK'),  # nazwa skryptu
        datetime.datetime.now(),  # bieżący czas
        lastvisit  # czas ostatniej wizyty
    )

    status = '200 OK'
    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body))),
                        ('Set-Cookie', 'lastvisit=' + str(datetime.datetime.now().strftime('%H-%M')))]
    start_response(status, response_headers)

    return [response_body]

if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('localhost', 4444, application)
    srv.serve_forever()
