#!/usr/bin/env python
# -*- coding=utf-8 -*-

import info


def application(environ, start_response):
    return [str(info.application(environ, start_response)[0]).upper()]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('localhost', 4444, application)
    srv.serve_forever()
