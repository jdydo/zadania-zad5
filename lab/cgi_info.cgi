#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cgi
import cgitb
cgitb.enable()
import os
import datetime


print "Content-Type: text/html"
print

body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Żądanie przyszło o %s.<br>
    </body>
</html>""" % (
        os.environ['SERVER_NAME'], # Nazwa serwera
        os.environ.get('SERVER_ADDR', 'BRAK'), # IP serwera
        os.environ.get('SERVER_PORT', 'BRAK'), # port serwera
        os.environ.get('REMOTE_USER', 'BRAK'), # nazwa klienta
        os.environ.get('REMOTE_ADDR', 'BRAK'), # IP klienta
        os.environ.get('REMOTE_PORT', 'BRAK'), # port klienta
        os.environ.get('SCRIPT_NAME', 'BRAK'), # nazwa skryptu
        datetime.datetime().now(), # czas
)

print body,