# -*- encoding: utf-8 -*-

import unittest
from server_methods import *


### Testy NIE WYMAGAJĄ włączonego serwera ###
class ServerOfflineTestCase(unittest.TestCase):

    def test_book_list(self):
        status, page = create_book_list_page()
        self.assertEqual(status, '200 OK')
        self.assertFalse('%s' in page)
        for book in BOOK_DB.titles():
            self.assertTrue(book['title'] in page)

    def test_book(self):
        environ = dict()
        environ['QUERY_STRING'] = 'id=id1'
        status, page = create_book_page(environ)
        self.assertEqual(status, '200 OK')
        self.assertFalse('%s' in page)
        self.assertTrue('CherryPy Essentials: Rapid Python Web Application Development' in page)
        self.assertTrue('978-1904811848' in page)
        self.assertTrue('Packt Publishing (March 31, 2007)' in page)
        self.assertTrue('Sylvain Hellegouarch' in page)

    def test_book_error_422(self):
        environ = dict()
        environ['QUERY_STRING'] = 'param=abc'
        status, page = create_book_page(environ)
        self.assertEqual(status, '422 Unprocessable Entity')
        self.assertFalse('%s' in page)
        self.assertTrue('422 Unprocessable Entity' in page)

    def test_book_error_404(self):
        environ = dict()
        environ['QUERY_STRING'] = 'id=id6'
        status, page = create_book_page(environ)
        self.assertEqual(status, '404 Not Found')
        self.assertFalse('%s' in page)
        self.assertTrue('404 Not Found' in page)

    def test_book_error_405(self):
        status, page = create_error_405_page()
        self.assertEqual(status, '405 Method Not Allowed')
        self.assertFalse('%s' in page)
        self.assertTrue('405 Method Not Allowed' in page)

    def test_book_error_500(self):
        status, page = create_error_500_page()
        self.assertEqual(status, '500 Internal Server Error')
        self.assertFalse('%s' in page)
        self.assertTrue('500 Internal Server Error' in page)

if __name__ == '__main__':
    unittest.main()
