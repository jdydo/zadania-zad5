# -*- encoding: utf-8 -*-

import unittest
from server_online_tests_methods import get_response, check_content
from server import HOST, PORT


### Testy WYMAGAJĄ włączonego serwera ###
class ServerOnlineTestCase(unittest.TestCase):

    def test_200a(self):
        status, reason, headers = get_response(HOST, PORT, 'GET', '/')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 200)
        self.assertEqual(reason, 'OK')
        self.assertEqual(content, True)

    def test_200b(self):
        status, reason, headers = get_response(HOST, PORT, 'GET', '/?id=id1')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 200)
        self.assertEqual(reason, 'OK')
        self.assertEqual(content, True)

    def test_404(self):
        status, reason, headers = get_response(HOST, PORT, 'GET', '/?id=id6')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 404)
        self.assertEqual(reason, 'Not Found')
        self.assertEqual(content, True)

    def test_422(self):
        status, reason, headers = get_response(HOST, PORT, 'GET', '/?param=abc')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 422)
        self.assertEqual(reason, 'Unprocessable Entity')
        self.assertEqual(content, True)

    def test_405(self):
        methods = ['POST', 'PUT', 'DELETE', 'HEAD']
        for method in methods:
            status, reason, headers = get_response(HOST, PORT, method)
            content = check_content(headers, 'text/html')
            self.assertEqual(status, 405)
            self.assertEqual(reason, 'Method Not Allowed')
            self.assertEqual(content, True)

    def test_headers(self):
        status, reason, headers = get_response(HOST, PORT, 'GET', '/')
        i = 0
        for header in headers:
            if 'server' in header[0] \
                    or 'date' in header[0] \
                    or 'content-type' in header[0] \
                    or 'content-length' in header[0] \
                    and header[1]:
                i += 1
        self.assertEqual(i, 4)
        self.assertEqual(len(headers), 4)

if __name__ == '__main__':
    unittest.main()
