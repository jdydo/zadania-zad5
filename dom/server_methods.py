#!/usr/bin/python
# -*- coding=utf-8 -*-

from cgi import escape
from urlparse import parse_qs
from bookdb import *


BOOK_DB = BookDB()


def create_book_page(environ):
    book_id = parse_qs(environ['QUERY_STRING']).get('id', None)
    if book_id:
        book_details = BOOK_DB.title_info(escape(book_id[0]))
        if book_details:
            return '200 OK', open('templates/book.html').read() % (
                book_details['title'], book_details['author'], book_details['publisher'], book_details['isbn']
            )
        else:
            return '404 Not Found', open('error_pages/404.html').read()
    else:
        return '422 Unprocessable Entity', open('error_pages/422.html').read()


def create_book_list_page():
    link = '<li style="margin-bottom: 10px;"><a href="/?id=%s">%s</a></li>'
    a = ''
    for book in BOOK_DB.titles():
        a += link % (book['id'], book['title'])
    return '200 OK', open('templates/list.html').read() % a


def create_error_405_page():
    return '405 Method Not Allowed', open('error_pages/405.html').read()


def create_error_500_page():
    return '500 Internal Server Error', open('error_pages/500.html').read()