# -*- encoding: utf-8 -*-

import httplib
import socket


# Metoda łączy z serwerem za pomocą HTTP (auto)
def get_response(host='localhost', port=4444, request_type='', page='/'):
    connection = httplib.HTTPConnection(host, port)

    try:
        connection.request(request_type, page)
        response = connection.getresponse()
    except:
        return '', '', ''
    finally:
        connection.close()

    return response.status, response.reason, response.getheaders()


# Metoda sprawdza nagłówek Content-Type
def check_content(headers='', content=''):
    guardian = False
    for header in headers:
        if ('content-type' in header) and (content in header[1]):
            guardian = True
    return guardian