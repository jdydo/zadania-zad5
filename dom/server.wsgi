#!/usr/bin/python
# -*- coding=utf-8 -*-

from wsgiref.simple_server import make_server
from server_methods import *
from bookdb import *


BOOK_DB = BookDB()
HOST = 'localhost'
PORT = 4444


def application(environ, start_response):
    try:
        if environ['REQUEST_METHOD'].upper() == 'GET' and environ.get('QUERY_STRING', None):
            status, page = create_book_page(environ)
        elif environ['REQUEST_METHOD'].upper() == 'GET':
            status, page = create_book_list_page()
        else:
            status, page = create_error_405_page()
    except Exception:
        status, page = create_error_500_page()

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(page)))]
    start_response(status, response_headers)

    return [page]


if __name__ == '__main__':
    srv = make_server(HOST, PORT, application)
    srv.serve_forever()